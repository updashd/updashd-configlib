<?php

use Updashd\Configlib\Config;

$publicKey = <<<KEY
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA024Bs7m7nss8Y2vXWj2o
OcWYljVEqODd0xHY52i/9Kjk4C9AuNQO+t59Ry1Qb1dMK4GZAT5vGjv92jUseq9I
awVEBukthXknIa2cTbolYu8HkE0l1WK2BU2WCkChB3DxYAbaiocYOvgh2kPWqqAC
NRzWdcvTld+7QQkTu2la3l8ijyeznIFV2Ev6A4TMbtXqb55uSKuUVxIXLhD3nilF
/odid1tVc7ide5K+erMivVsRrovs1qhlRFo8scrSXWOacJFGObgqF+czpX0Sfztx
AneWcNaSN7L360bD0P0Oga0Jq3i4xUCRbjWfwibMhgOJ6B8tm/HI0Vrk1eZELoh3
TwIDAQAB
-----END PUBLIC KEY-----
KEY
;

$privateKey = <<<KEY
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDTbgGzubueyzxj
a9daPag5xZiWNUSo4N3TEdjnaL/0qOTgL0C41A763n1HLVBvV0wrgZkBPm8aO/3a
NSx6r0hrBUQG6S2FeSchrZxNuiVi7weQTSXVYrYFTZYKQKEHcPFgBtqKhxg6+CHa
Q9aqoAI1HNZ1y9OV37tBCRO7aVreXyKPJ7OcgVXYS/oDhMxu1epvnm5Iq5RXEhcu
EPeeKUX+h2J3W1VzuJ17kr56syK9WxGui+zWqGVEWjyxytJdY5pwkUY5uCoX5zOl
fRJ/O3ECd5Zw1pI3svfrRsPQ/Q6BrQmreLjFQJFuNZ/CJsyGA4noHy2b8cjRWuTV
5kQuiHdPAgMBAAECggEBAJRg6m7gFOzPApVfdGXPSYUpx/j6bzH5hZOAPfHBEJ25
/c3bO5e6sryacd8GzSEmab1KmT9HOp91pKSDlrCO3MrGikQzVpAKQ00eUsg1r9Er
oOpDSh7jxIwaNAcD7j6To8HZbXu0bWvNAaz02O0rmAEAOP24SsgumHJ+1Us8F9pg
MOV5cEU0q5FeMyQnAjsziARaVT0AV6JWlApjNdPKz9GkgPbfBAwYwfJceC5lL0Ns
O20w8E5ksh6GukU+N5JY8ceIBGOG7LV9j2B3e6u1GDV415C1HFbOqiC0+cClRRZH
ILro49Bhvlhq2G+Vef2owfZShhqsLN5WJBftg0pAWskCgYEA787f/d4x2/xSzasL
ynOtDBEh3KKeLlDXRTqQIEJ6/I39oSizS20BN/9wRortgbOKvaO9Hu2JHMnqD0NH
k334S5LhLT6cXXd9D6ABScAhgS6pCx/RPo9OFtCWRPZYNgOonvYv+3zKuKC9/enm
uNPPZBGalWudSSZLqwz60ECmywMCgYEA4bSbMPHUjP1X2s5tih1euBYagh9+vJyf
pyBGetg08Xn/pYOIvG9Gp9IShjbdg9MB7xO3kZ3/PIbVrQAfr7oj7whKquodGEuC
JG7jjTGw8dKLBDmcm0m1sBOH6SmWWPnqqaBw+8tD67nwxGyeq0PNXfGFk8pejbbP
M9QqVke1asUCgYEAvkgmz+gZI/cpNOy+JFx5ORP3DH00ioe26MW5Sv+1WuTIFbKX
4geSaf6T9kWSg9IVCrj7/ah5nZBR2t6F0vCnrj/T0PTXWgrP/y/1FbIQY9LEa4FJ
pRxM2CmWsw3NnL52c/vgwuqYaLuTRgDtBEd9TmfxgxKq8Toj3IwmEEzcjEUCgYAi
C/p1EaW8duLv0L/ZW8DUCBGLm3CkxwOWVFoNYdvqqTqVHxhbRHXmDtxYfqI/pm7e
P50rTCgaW0SUSjZ9bZjKD/gjfBlTduB02n1c6SgF8nqh8obbLe1IWEniNDHHzjtF
xzwD34+pWW0mVDRDJYEvMKWzu40z12W+Lb0NMtf8+QKBgAV2DLsgI6S8lzV4Ya5a
uAywYPl2jBm2D2j1yd3Y9/rSwmQdhZ0lGfMlQ7sDiGC4UeI21/MUv1fOejpfE5gl
Qejr9Gno+na/7A6S42ilZa9ZRoIb75eEtAFFD5b+SKbgrQU5S6mQmf5/pbkD6bvg
zyXhpfdJmG7AC6NutqSWxjzx
-----END PRIVATE KEY-----
KEY
;

require __DIR__ . '/../vendor/autoload.php';

$config = new Config();

$config->addFieldPassword('password', 'Password', null, true)
    ->setUseEncryption();


$preconf = clone $config;

$preconf->setValue('password', 'test123');

$preconf->setPublicEncryptKey($publicKey);

$json = $preconf->toJson();

echo $json . PHP_EOL;

$postconf = clone $config;

$postconf->fromJson($json);

print_r($postconf->getValue('password')); echo PHP_EOL;
print_r($postconf->getField('password')->getEncryptedValue()); echo PHP_EOL;

$postconf->setPrivateDecryptKey($privateKey);

$postconf->fromJson($json);

print_r($postconf->getValue('password')); echo PHP_EOL;
print_r($postconf->getField('password')->getEncryptedValue()); echo PHP_EOL;