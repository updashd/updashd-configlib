<?php

require __DIR__ . '/../vendor/autoload.php';

$config = new \Updashd\Configlib\Config();

$config->addFieldText('user_name', 'User Name', 'Default', true);

$config->setValue('user_name', $config::REFERENCE_FIELD_MARKER . 'user');

$jsonConfig = $config->toJson();

// This is what would be stored in the database
echo $jsonConfig . PHP_EOL;

///////////////////////////////////
// This is what the agent will do
///////////////////////////////////

// jsonConfig would have been loaded from the database
$jsonConfig = $config->fillReferenceFields($jsonConfig, [
    'user' => 'Updashd'
]);


////////////////////////////////////////////
// This is what the worker does internally
////////////////////////////////////////////
$config->fromJson($jsonConfig);

print_r($config->getValue('user_name', true));