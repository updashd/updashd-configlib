<?php
require __DIR__ . '/../vendor/autoload.php';

echo '===Creating Fields===' . PHP_EOL;
$config = new \Updashd\Configlib\Config();
echo '===Host Name===' . PHP_EOL;
$config->addFieldText('hostname', 'Host Name')
    ->addValidator(new \Updashd\Configlib\Validator\HostnameValidator());

echo '===IP===' . PHP_EOL;
$config->addFieldText('ip', 'IP')
    ->addValidator(new \Updashd\Configlib\Validator\Ip4Validator());

echo '===Port===' . PHP_EOL;
$config->addFieldText('port', 'Port')
    ->addValidator(new \Updashd\Configlib\Validator\NumberValidator(false, 1, 65535));

echo '===Method===' . PHP_EOL;
$config->addFieldSelect('method', 'Method', ['GET' => 'GET', 'POST' => 'POST']);

echo '===Should all be false:===' . PHP_EOL;

foreach ($config->getFields() as $field) {
    $isValid = $field->isValid();
    
    echo $field->getLabel() . ': ' . ($isValid ? 'true' : 'false') . PHP_EOL;
    
    if (! $isValid) {
        echo implode(PHP_EOL, $field->getMessages()) . PHP_EOL;
        echo PHP_EOL;
    }
}

echo PHP_EOL . '===Should be true:===' . PHP_EOL;

$config->setValue('hostname', 'updashd.com');
$config->setValue('ip', '192.241.170.194');
$config->setValue('port', 1);
$config->setValue('method', 'GET');

foreach ($config->getFields() as $field) {
    $isValid = $field->isValid();
    
    echo $field->getLabel() . ': ' . ($isValid ? 'true' : 'false') . PHP_EOL;
    
    if (! $isValid) {
        echo implode(PHP_EOL, $field->getMessages()) . PHP_EOL;
        echo PHP_EOL;
    }
}