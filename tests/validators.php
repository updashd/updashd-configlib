<?php

require __DIR__ . '/../vendor/autoload.php';

$things = [
    null,
    '',
    '0',
    '100',
    '0983',
    'blog',
    'Jeff91',
    'This is a long sentence',
    '19.64',
    '19.23.234.55',
    '192.168.0.101',
    '2001:0db8:85a3:0000:0000:8a2e:0370:7334'
];

$validators = [
    'Regex' => new \Updashd\Configlib\Validator\RegexValidator('/^[0-9]+$/'),
    'Number' => new \Updashd\Configlib\Validator\NumberValidator(),
    'IPv4' => new \Updashd\Configlib\Validator\Ip4Validator(),
    'Host Name' => new \Updashd\Configlib\Validator\HostnameValidator(),
];

/**
 * @var string $validatorName
 * @var Updashd\Configlib\Validator\AbstractValidator $validator
 */
foreach ($validators as $validatorName => $validator) {
    echo $validatorName . PHP_EOL;
    
    foreach ($things as $thing) {
        $isValid = $validator->isValid($thing);
        printf("%s: %s %s\n", $thing, $isValid ? 'true' : 'false!', !$isValid ? ' Reason: ' . $validator->getMessagesString() : '');
    }
    
    echo PHP_EOL;
}