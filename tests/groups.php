<?php
require __DIR__ . '/../vendor/autoload.php';

$config = new Updashd\Configlib\Config();
$config->addGroup('basic', 'Basic');

$config->addFieldText('text', 'Text', '');
$config->addFieldToGroup('basic', 'text');

$config->addFieldNumber('number', 'Number');
$config->addFieldToGroup('basic', 'number');

$config->addFieldPassword('password', 'Password');
$config->addFieldToGroup('basic', 'password');

$config->addFieldCheckbox('checkbox', 'Checkbox');
$config->addFieldToGroup('basic', 'checkbox');

$config->addGroup('advanced', 'Advanced');

$config->addFieldUrl('url', 'URL');
$config->addFieldToGroup('advanced', 'url');

$config->addFieldMultiLineText('multi-line', 'Multi-Line', '');
$config->addFieldToGroup('advanced', 'multi-line');

$config->addFieldMultiCheckbox('multi-checkbox', 'Multi-Checkbox', [
    '1' => '1',
    '2' => '2',
    '3' => '3',
    '4' => '4',
    '5' => '5',
]);
$config->addFieldToGroup('advanced', 'multi-checkbox');

$config->addFieldRadio('radio', 'Radio', [
    '1' => '1',
    '2' => '2',
    '3' => '3',
    '4' => '4',
    '5' => '5',
]);
$config->addFieldToGroup('advanced', 'radio');

$config->addFieldSelect('select', 'Select', [
    '1' => '1',
    '2' => '2',
    '3' => '3',
    '4' => '4',
    '5' => '5',
]);
$config->addFieldToGroup('advanced', 'select');

foreach ($config->getGroups() as $group) {
    echo $group->getLabel() . PHP_EOL;

    foreach ($group->getFields() as $field) {
        echo '    ' .  $field->getLabel() . ' = ' . $field->getValue() . PHP_EOL;
    }
}

$config->setValue('text', 'Greetings');
$config->setValue('number', 2.0);

foreach ($config->getGroups() as $group) {
    echo $group->getLabel() . PHP_EOL;

    foreach ($group->getFields() as $field) {
        echo '    ' .  $field->getLabel() . ' = ' . $field->getValue() . PHP_EOL;
    }
}