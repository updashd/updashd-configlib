<?php
namespace Updashd\Configlib\Cryptography;

use Updashd\Configlib\Exception\ConfigurationException;

class OpenSsl {
    const ENCRYPTED_PREFIX = '__ENCRYPTED__';

    //Block size for encryption block cipher
    const ENCRYPT_BLOCK_SIZE = 200;// this for 2048 bit key for example, leaving some room

    //Block size for decryption block cipher
    const DECRYPT_BLOCK_SIZE = 256;// this again for 2048 bit key

    public static function encrypt ($plainData, $publicPEMKey) {
        $publicKey = openssl_get_publickey($publicPEMKey);

        if ($publicKey === false) {
            throw new ConfigurationException('Unable to use Public Encryption Key! Either one was not provided or it was invalid.');
        }

        $encrypted = '';

        $plainData = str_split($plainData, self::ENCRYPT_BLOCK_SIZE);

        foreach ($plainData as $chunk) {
            $partialEncrypted = '';

            //using for example OPENSSL_PKCS1_PADDING as padding
            $encryptionOk = openssl_public_encrypt($chunk, $partialEncrypted, $publicKey, OPENSSL_PKCS1_PADDING);

            if ($encryptionOk === false) {
                throw new ConfigurationException('Unable to encrypt data!');
            }

            $encrypted .= $partialEncrypted;
        }

        $base64 = base64_encode($encrypted);

        openssl_pkey_free($publicKey);

        // Add encrypted prefix
        return self::ENCRYPTED_PREFIX . $base64;
    }

    public static function decrypt ($data, $privatePEMKey) {
        $data = self::stripPrefix($data);

        $privateKey = openssl_get_privatekey($privatePEMKey);

        if ($privateKey === false) {
            throw new ConfigurationException('Unable to use Private Decryption Key! Either one was not provided or it was invalid.');
        }

        $decrypted = '';

        //decode must be done before splitting for getting the binary String
        $data = str_split(base64_decode($data), self::DECRYPT_BLOCK_SIZE);

        foreach ($data as $chunk) {
            $partial = '';

            //be sure to match padding
            $decryptionOK = openssl_private_decrypt($chunk, $partial, $privateKey, OPENSSL_PKCS1_PADDING);

            if ($decryptionOK === false) {
                return false;
            }

            //here also processed errors in decryption. If too big this will be false
            $decrypted .= $partial;
        }

        openssl_pkey_free($privateKey);

        return $decrypted;
    }

    public static function hasPrefix (&$input) {
        return strpos($input, self::ENCRYPTED_PREFIX) === 0;
    }

    public static function stripPrefix ($input) {
        if (self::hasPrefix($input)) {
            $input = substr($input, strlen(self::ENCRYPTED_PREFIX));
        }

        return $input;
    }
}