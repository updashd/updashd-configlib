<?php
namespace Updashd\Configlib;

use Updashd\Configlib\Validator\InArrayValidator;

class Field {

    const FIELD_TYPE_TEXT = 'text';
    const FIELD_TYPE_NUMBER = 'number';
    const FIELD_TYPE_PASSWORD = 'password';
    const FIELD_TYPE_URL = 'url';
    const FIELD_TYPE_CHECKBOX = 'checkbox';
    const FIELD_TYPE_MULTI_LINE_TEXT = 'multi_line_text';

    const FIELD_TYPE_RADIO = 'radio';
    const FIELD_TYPE_SELECT = 'select';
    const FIELD_TYPE_MULTI_CHECKBOX = 'multi_checkbox';

    const OPTION_TYPE_SIMPLE = 'simple';
    const OPTION_TYPE_OPT_GROUP = 'opt_group';

    private $type = self::FIELD_TYPE_TEXT;
    private $value;
    private $defaultValue;
    private $referenceField;
    private $optionsType = self::OPTION_TYPE_SIMPLE;
    private $options = null;
    private $label;
    private $isRequired = false;
    private $useEncryption = false;
    private $encryptedValue;
    private $validators = [];
    private $messages = [];

    public function __construct ($type, $label, $options = null, $isRequired = false) {
        $this->setType($type);
        $this->setLabel($label);
        $this->setOptions($options);
        $this->setIsRequired($isRequired);
    }

    /**
     * @return string
     */
    public function getType () {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType ($type) {
        $this->type = $type;
        return $this;
    }

    /**
     * Determine if this field is a multi-value field.
     * @return bool
     */
    public function isMulti () {
        return in_array($this->getType(), [
            self::FIELD_TYPE_MULTI_CHECKBOX
        ]);
    }

    /**
     * @return mixed
     */
    public function getDefaultValue () {
        return $this->defaultValue;
    }

    /**
     * @param mixed $defaultValue
     * @return $this
     */
    public function setDefaultValue ($defaultValue) {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @param bool $canUseDefault
     * @return string
     */
    public function getValue ($canUseDefault = false) {
        if ($canUseDefault && $this->value === null) {
            return $this->getDefaultValue();
        }
        else {
            return $this->value;
        }
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue ($value) {
        $this->value = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReferenceField () {
        return $this->referenceField;
    }

    /**
     * @param mixed $referenceField
     * @return $this
     */
    public function setReferenceField ($referenceField) {
        $this->referenceField = $referenceField;
        return $this;
    }

    /**
     * @return string
     */
    public function getOptionsType () {
        return $this->optionsType;
    }

    /**
     * @param string $optionsType
     * @return $this
     */
    public function setOptionsType ($optionsType) {
        $this->optionsType = $optionsType;
        return $this;
    }

    /**
     * @return array
     */
    public function getOptions () {
        return $this->options;
    }

    /**
     * @param array $options
     * @param bool $useInArrayValidator
     * @return $this
     */
    public function setOptions ($options, $useInArrayValidator = true) {
        if (is_array($options)) {
            if ($this->getOptionsType() == self::OPTION_TYPE_SIMPLE) {
                $validOptions = $options;
            }
            else {
                $validOptions = [];

                foreach ($options as $optGroup => $subOptions) {
                    $validOptions[] = $subOptions;
                }
            }

            if ($useInArrayValidator) {
                $this->clearValidators();
                $validOptions = array_keys($validOptions);
                $this->addValidator(new InArrayValidator($validOptions));
            }
        }

        $this->options = $options;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel () {
        return $this->label;
    }

    /**
     * @param string $label
     * @return $this
     */
    public function setLabel ($label) {
        $this->label = $label;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsRequired () {
        return $this->isRequired;
    }

    /**
     * @param bool $isRequired
     * @return $this
     */
    public function setIsRequired ($isRequired) {
        $this->isRequired = $isRequired;
        return $this;
    }

    /**
     * @return array
     */
    public function getValidators () {
        return $this->validators;
    }

    /**
     * @param array $validators
     * @return $this
     */
    public function setValidators ($validators) {
        $this->validators = $validators;
        return $this;
    }

    /**
     * Add a validator to the field.
     * @param Validator\AbstractValidator $validator
     * @param string|null $message
     * @return $this
     */
    public function addValidator (Validator\AbstractValidator $validator, $message = null) {
        if ($message) {
            $validator->setCustomMessage($message);
        }

        $this->validators[] = $validator;
        return $this;
    }

    /**
     * Clear all validators from a field.
     * @return $this
     */
    public function clearValidators () {
        $this->setValidators([]);
        return $this;
    }

    /**
     * Remove a specific validator by index.
     * @param $index
     * @return bool
     */
    public function removeValidator ($index) {
        if (isset($this->validators[$index])) {
            unset($this->validators);
            return true;
        }

        return false;
    }

    /**
     * Check if the field's value is valid.
     * @param bool $canUseDefault
     * @return bool
     */
    public function isValid ($canUseDefault = false) {
        $this->clearMessages();

        $isValid = true;

        if ($this->getIsRequired() && ($this->getValue($canUseDefault) === null && ! $this->getReferenceField()) && ! $this->hasEncryptedValue()) {
            $isValid = false;
            $this->addMessage('Field is required');
        }
        else if ($this->getValue($canUseDefault) !== null) {
            /** @var Validator\AbstractValidator $validator */
            foreach ($this->getValidators() as $validator) {
                if (! $validator->isValid($this->getValue($canUseDefault))) {
                    $isValid = false;
                    $this->addMessages($validator->getMessages());
                }
            }
        }

        return $isValid;
    }

    /**
     * @return array
     */
    public function getMessages () {
        return $this->messages;
    }

    /**
     * @param array $messages
     * @return $this
     */
    public function setMessages ($messages) {
        $this->messages = $messages;
        return $this;
    }

    public function clearMessages () {
        $this->setMessages([]);
        return $this;
    }

    /**
     * Add a message to the field
     * @param string $message
     * @return $this
     */
    public function addMessage ($message) {
        $this->messages[] = $message;
        return $this;
    }

    /**
     * Add an array of messages to the field
     * @param array $messages
     * @return $this
     */
    public function addMessages ($messages) {
        foreach ($messages as $message) {
            $this->addMessage($message);
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function getUseEncryption () {
        return $this->useEncryption;
    }

    /**
     * @param bool $useEncryption
     * @return $this
     */
    public function setUseEncryption ($useEncryption = true) {
        $this->useEncryption = $useEncryption;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEncryptedValue () {
        return $this->encryptedValue;
    }

    /**
     * @param mixed $encryptedValue
     * @return $this
     */
    public function setEncryptedValue ($encryptedValue) {
        $this->encryptedValue = $encryptedValue;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasEncryptedValue () {
        return $this->encryptedValue !== null;
    }
}