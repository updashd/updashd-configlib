<?php
namespace Updashd\Configlib\Validator;

class RegexValidator extends AbstractValidator {
    const MESSAGE = "Input must match Regular Expression: ";
    
    protected $pattern;
    
    public function __construct ($pattern) {
        $this->setPattern($pattern);
    }
    
    public function isValid ($input) {
        $isValid = parent::isValid($input);
        
        $hasMatch = preg_match($this->getPattern(), $input);
        
        if ($hasMatch === FALSE) {
            $isValid = false;
            $this->addMessage('preg_match failed with error code: ' . preg_last_error());
        }
        
        if (! $hasMatch) {
            $isValid = false;
            $this->addMessage(self::MESSAGE . $this->getPattern());
        }
        
        return $isValid;
    }
    
    /**
     * @return mixed
     */
    public function getPattern () {
        return $this->pattern;
    }
    
    /**
     * @param mixed $pattern
     */
    public function setPattern ($pattern) {
        $this->pattern = $pattern;
    }
}