<?php
namespace Updashd\Configlib\Validator;

class PortNumberValidator extends NumberValidator {
    const MESSAGE = 'Input is not a valid port number.';
    
    public function __construct () {
        parent::__construct(false, 1, 65535);
    }
    
    public function isValid ($input) {
        $isValid = parent::isValid($input);
        
        if (! $isValid) {
            $this->clearMessages();
            $this->setMessage(self::MESSAGE);
        }

        return $isValid;
    }
}