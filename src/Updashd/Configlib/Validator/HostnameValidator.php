<?php
namespace Updashd\Configlib\Validator;

class HostnameValidator extends RegexValidator {
    const REGEX_PATTERN = '/^(?=.{1,255}$)[0-9A-Za-z](?:(?:[0-9A-Za-z]|-){0,61}[0-9A-Za-z])?(?:\.[0-9A-Za-z](?:(?:[0-9A-Za-z]|-){0,61}[0-9A-Za-z])?)*\.?$/';
    const MESSAGE = 'Input must be an IETF RFC1123 compliant hostname.';
    
    public function __construct () {
        parent::__construct(self::REGEX_PATTERN);
    }
    
    public function isValid ($input) {
        $isValid = parent::isValid($input);
        
        if (! $isValid) {
            $this->clearMessages();
            $this->setMessage(self::MESSAGE);
        }
        
        return $isValid;
    }
}