<?php
namespace Updashd\Configlib\Validator;

class NumberValidator extends AbstractValidator {
    protected $allowDecimal;
    protected $min;
    protected $max;

    public function __construct ($allowDecimal = true, $min = null, $max = null) {
        $this->setAllowDecimal($allowDecimal);
        $this->setMin($min);
        $this->setMax($max);
    }

    public function isValid ($input) {
        $isValid = parent::isValid($input);

        $allowDecimal = $this->getAllowDecimal();
        $max = $this->getMax();
        $min = $this->getMin();

        if (! is_numeric($input)) {
            $isValid = false;
            $this->addMessage('Input must be numeric.');
        }

        if (! $allowDecimal && is_float($input)) {
            $isValid = false;
            $this->addMessage('Decimals are not allowed.');
        }

        if ($min !== null && $min > $input) {
            $isValid = false;
            $this->addMessage('Input value is too small. Min: ' . $min);
        }

        if ($max !== null && $max < $input) {
            $isValid = false;
            $this->addMessage('Input value is too large. Max: ' . $max);
        }

        return $isValid;
    }

    /**
     * @return bool
     */
    public function getAllowDecimal () {
        return $this->allowDecimal;
    }

    /**
     * @param bool $allowDecimal
     */
    public function setAllowDecimal ($allowDecimal) {
        $this->allowDecimal = $allowDecimal;
    }

    /**
     * @return mixed
     */
    public function getMin () {
        return $this->min;
    }

    /**
     * @param mixed $min
     */
    public function setMin ($min) {
        $this->min = $min;
    }

    /**
     * @return mixed
     */
    public function getMax () {
        return $this->max;
    }

    /**
     * @param mixed $max
     */
    public function setMax ($max) {
        $this->max = $max;
    }
}