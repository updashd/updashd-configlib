<?php
namespace Updashd\Configlib\Validator;

class Ip4Validator extends RegexValidator {
    const REGEX_PATTERN = '/^(([1-9]?\d|1\d\d|25[0-5]|2[0-4]\d)\.){3}([1-9]?\d|1\d\d|25[‌0-5]|2[0-4]\d)$/';
    const MESSAGE = 'Input must be a valid IPv4 Address.';
    
    public function __construct () {
        parent::__construct(self::REGEX_PATTERN);
    }
    
    public function isValid ($input) {
        $isValid = parent::isValid($input);

        if (! $isValid) {
            $this->clearMessages();
            $this->setMessage(self::MESSAGE);
        }

        return $isValid;
    }
}