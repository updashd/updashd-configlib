<?php
namespace Updashd\Configlib\Validator;

abstract class AbstractValidator {
    protected $messages = [];
    protected $customMessage = null;
    
    const MESSAGE = 'Input is invalid.';
    
    /**
     * Check if the input was valid. This will always be true.
     * @param $input
     * @return bool
     */
    public function isValid ($input) {
        $this->clearMessages();
        
        return true;
    }
    
    /**
     * @return array
     */
    public function getMessages () {
        return $this->messages;
    }
    
    /**
     * @param string $glue
     * @return array
     */
    public function getMessagesString ($glue = PHP_EOL) {
        return implode($glue, $this->getMessages());
    }
    
    public function addMessage ($message) {
        $customMessage = $this->getCustomMessage();
        
        if ($customMessage) {
            $this->messages = [$message];
        }
        else {
            $this->messages[] = $message;
        }
    }
    
    public function clearMessages () {
        $this->messages = [];
    }
    
    public function setMessage ($message) {
        $this->clearMessages();
        $this->addMessage($message);
    }
    
    /**
     * @return string
     */
    public function getCustomMessage () {
        return $this->customMessage;
    }
    
    /**
     * @param string $customMessage
     */
    public function setCustomMessage ($customMessage) {
        $this->customMessage = $customMessage;
    }
}