<?php
namespace Updashd\Configlib\Validator;

class InArrayValidator extends AbstractValidator {
    const MESSAGE = 'Input is not a valid option.';

    protected $options = null;

    /**
     * InArrayValidator constructor.
     * @param array $options Allowed options
     */
    public function __construct ($options) {
        $this->setOptions($options);
    }

    public function isValid ($input) {

        $isValid = parent::isValid($input);

        // Missing all together
        if ($input === null) {
            $this->setMessage(self::MESSAGE);
        }
        // Array to Array
        elseif (is_array($input) && array_diff($input, $this->getOptions())) {
            $this->setMessage(self::MESSAGE);
        }
        // String and Array
        elseif (!is_array($input) && ! in_array($input, $this->getOptions())) {
            $isValid = false;
            $this->setMessage(self::MESSAGE);
        }

        return $isValid;
    }

    /**
     * @return array
     */
    public function getOptions () {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions ($options) {
        $this->options = $options;
    }
}