<?php
namespace Updashd\Configlib;

class Group {
    protected $label;
    protected $fields = [];

    /**
     * Group constructor.
     * @param string $label
     */
    public function __construct ($label) {
        $this->setLabel($label);
    }

    /**
     * @param string $label
     */
    public function setLabel ($label) {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getLabel () {
        return $this->label;
    }

    /**
     * @param string $fieldKey
     * @param Field $fieldObj
     */
    public function addField ($fieldKey, $fieldObj) {
        $this->fields[$fieldKey] = $fieldObj;
    }

    /**
     * @param string $fieldKey
     * @return Field|null
     */
    public function getField ($fieldKey) {
        if (array_key_exists($fieldKey, $this->fields)) {
            return $this->fields[$fieldKey];
        }

        return null;
    }

    /**
     * @param string $fieldKey
     * @return bool
     */
    public function removeField ($fieldKey) {
        if (array_key_exists($fieldKey, $this->fields)) {
            unset($this->fields[$fieldKey]);
            return true;
        }

        return false;
    }

    /**
     * @return Field[]
     */
    public function getFields () {
        return $this->fields;
    }
}