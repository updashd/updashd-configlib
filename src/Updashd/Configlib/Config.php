<?php
namespace Updashd\Configlib;

use Updashd\Configlib\Cryptography\OpenSsl;
use Updashd\Configlib\Exception\ConfigurationException;

class Config {
    const REFERENCE_FIELD_MARKER = "\f";

    /** @var Field[] */
    private $fields = [];
    /** @var Group[]  */
    private $groups = [];

    private $publicEncryptKey;
    private $privateDecryptKey;

    public function addFieldText ($key, $label, $defaultValue = null, $isRequired = false) {
        return $this->addFieldType($key, Field::FIELD_TYPE_TEXT, $label, $defaultValue, $isRequired);
    }

    public function addFieldMultiLineText ($key, $label, $defaultValue = null, $isRequired = false) {
        return $this->addFieldType($key, Field::FIELD_TYPE_MULTI_LINE_TEXT, $label, $defaultValue, $isRequired);
    }

    public function addFieldNumber ($key, $label, $defaultValue = null, $isRequired = false) {
        return $this->addFieldType($key, Field::FIELD_TYPE_NUMBER, $label, $defaultValue, $isRequired);
    }

    public function addFieldPassword ($key, $label, $defaultValue = null, $isRequired = false) {
        return $this->addFieldType($key, Field::FIELD_TYPE_PASSWORD, $label, $defaultValue, $isRequired);
    }

    public function addFieldUrl ($key, $label, $defaultValue = null, $isRequired = false) {
        return $this->addFieldType($key, Field::FIELD_TYPE_URL, $label, $defaultValue, $isRequired);
    }

    public function addFieldCheckbox ($key, $label, $defaultValue = false, $isRequired = false) {
        return $this->addFieldType($key, Field::FIELD_TYPE_CHECKBOX, $label, $defaultValue, $isRequired);
    }

    public function addFieldRadio ($key, $label, $options, $defaultValue = null, $isRequired = false, $useInArrayValidator = true) {
        $field = $this->addFieldType($key, Field::FIELD_TYPE_RADIO, $label, $defaultValue, $isRequired);
        $field->setOptions($options, $useInArrayValidator);
        return $field;
    }

    public function addFieldSelect ($key, $label, $options, $defaultValue = null, $isRequired = false, $useInArrayValidator = true) {
        $field = $this->addFieldType($key, Field::FIELD_TYPE_SELECT, $label, $defaultValue, $isRequired);
        $field->setOptions($options, $useInArrayValidator);
        return $field;
    }

    public function addFieldMultiCheckbox ($key, $label, $options, $defaultValue = null, $isRequired = false, $useInArrayValidator = true) {
        $field = $this->addFieldType($key, Field::FIELD_TYPE_MULTI_CHECKBOX, $label, $defaultValue, $isRequired);
        $field->setOptions($options, $useInArrayValidator);
        return $field;
    }

    public function addFieldType ($key, $type, $label, $defaultValue = null, $isRequired = false) {
        $field = new Field($type, $label);

        $field->setIsRequired($isRequired);

        if ($defaultValue !== null) {
            $field->setDefaultValue($defaultValue);
        }

        return $this->addField($key, $field);
    }

    /**
     * @param string $key
     * @param Field $field
     * @return Field
     */
    public function addField ($key, $field) {
        $this->fields[$key] = $field;

        return $field;
    }

    /**
     * Get a field in the config
     * @param $key
     * @return null|Field
     */
    public function getField ($key) {
        if (array_key_exists($key, $this->fields)) {
            return $this->fields[$key];
        }

        return null;
    }

    /**
     * Set the value of a given field
     * @param string $key
     * @param mixed $value
     * @throws \Exception
     */
    public function setValue ($key, $value) {
        $field = $this->getField($key);

        if ($field) {
            $field->setValue($value);
        }
        else {
            throw new \Exception('Field not found: ' . $key);
        }
    }

    /**
     * Set the default value of a given field
     * @param string $key
     * @param mixed $defaultValue
     * @throws \Exception
     */
    public function setDefaultValue ($key, $defaultValue) {
        $field = $this->getField($key);

        if ($field) {
            $field->setDefaultValue($defaultValue);
        }
        else {
            throw new \Exception('Field not found: ' . $key);
        }
    }

    /**
     * Get the value of a given field
     * @param string $key
     * @param bool $canUseDefault
     * @return null|string
     * @throws \Exception
     */
    public function getValue ($key, $canUseDefault = false) {
        $field = $this->getField($key);

        if ($field) {
            return $field->getValue($canUseDefault);
        }
        else {
            throw new \Exception('Field not found: ' . $key);
        }
    }

    /**
     * Retrieve a configuration value, throwing an exception if no value is set.
     * @param string $key
     * @param bool $canUseDefault
     * @return null|string
     * @throws ConfigurationException
     */
    public function getValueRequired ($key, $canUseDefault = true) {
        $value = $this->getValue($key, $canUseDefault);

        if ($value === null) {
            throw new ConfigurationException('Configuration key missing: "' . $key . '"', 0);
        }

        return $value;
    }

    /**
     * Set the value of a reference field
     * @param $fieldKey
     * @param $value
     * @return int number of fields that used the reference field
     * @internal param $field
     */
    public function setReferenceValue ($fieldKey, $value) {
        $count = 0;

        foreach ($this->getFields() as $field) {
            if ($field->getReferenceField() == $fieldKey) {
                $field->setValue($value);
                $count++;
            }
        }

        return $count;
    }

    /**
     * @return Field[]
     */
    public function getFields () {
        return $this->fields;
    }

    /**
     * @param string $groupKey
     * @param string $label
     * @return Group
     */
    public function addGroup ($groupKey, $label) {
        $group = new Group($label);
        $this->groups[$groupKey] = $group;
        return $group;
    }

    /**
     * Add a field to a group.
     * @param string $groupKey
     * @param string $fieldKey
     * @throws ConfigurationException
     */
    public function addFieldToGroup ($groupKey, $fieldKey) {
        $group = $this->getGroup($groupKey);

        if (! $group) {
            throw new ConfigurationException('Cannot add field to group: group does not exist.');
        }

        $field = $this->getField($fieldKey);

        if (! $field) {
            throw new ConfigurationException('Cannot add field to group: field does not exist.');
        }

        $group->addField($fieldKey, $field);

        $this->setGroup($groupKey, $group);
    }

    /**
     * Remove a field from a group.
     * @param string $groupKey
     * @param string $fieldKey
     * @throws ConfigurationException
     */
    public function removeFieldFromGroup ($groupKey, $fieldKey) {
        $group = $this->getGroup($groupKey);

        if (! $group) {
            throw new ConfigurationException('Cannot remove field from group: group does not exist.');
        }

        $group->removeField($fieldKey);

        $this->setGroup($groupKey, $group);
    }

    /**
     * @return Group[]
     */
    public function getGroups () {
        return $this->groups;
    }

    /**
     * @param string $groupKey
     * @param Group $group
     */
    public function setGroup ($groupKey, $group) {
        $this->groups[$groupKey] = $group;
    }

    /**
     * @param $groupKey
     * @return Group
     * @throws ConfigurationException
     */
    public function getGroup ($groupKey) {
        if (array_key_exists($groupKey, $this->groups)) {
            return $this->groups[$groupKey];
        }

        return null;
    }

    /**
     * Encode the object into json
     * @return string
     */
    public function toJson () {
        $result = array();

        foreach ($this->getFields() as $fieldKey => $fieldOptions) {
            // Encrypt new values
            if ($fieldOptions->getUseEncryption()) {
                $data = $fieldOptions->getEncryptedValue();

                // Re-encrypt a new value
                if ($fieldOptions->getValue(true) !== null) {
                    $value = $fieldOptions->getValue(true);

                    if ($fieldOptions->isMulti()) {
                        $value = json_encode($value);
                    }

                    $data = OpenSsl::encrypt($value, $this->getPublicEncryptKey());

                    // Set the encrypted value
                    $fieldOptions->setEncryptedValue($data);

                    // Null out the old value
                    $fieldOptions->setValue(null);
                }

                $result[$fieldKey] = $data;
            }
            // Referenced fields
            elseif ($fieldOptions->getReferenceField()) {
                $result[$fieldKey] = self::REFERENCE_FIELD_MARKER . $fieldOptions->getReferenceField();
            }
            // Normal fields
            elseif ($fieldOptions->getValue() !== null) {
                $result[$fieldKey] = $fieldOptions->getValue();
            }
        }

        return json_encode($result);
    }

    /**
     * This function will read configuration values from a json-encoded string. The Config object must already have fields added.
     * @param string $json
     * @return array
     * @throws \Exception
     */
    public function fromJson ($json) {
        $data = json_decode($json, true);

        if ($data === FALSE) {
            throw new ConfigurationException('Json could not be decoded: ' . json_last_error_msg(), json_last_error());
        }

        if (is_array($data)) {
            foreach ($data as $fieldKey => $fieldValue) {
                $field = $this->getField($fieldKey);

                if ($field) {
                    // If the value is encrypted
                    if (is_string($fieldValue) && OpenSsl::hasPrefix($fieldValue)) {
                        $field = $this->getField($fieldKey);
                        $field->setEncryptedValue($fieldValue);
                        $field->setUseEncryption(true);

                        $decryptKey = $this->getPrivateDecryptKey();

                        if ($decryptKey) {
                            $value = OpenSsl::decrypt($fieldValue, $decryptKey);

                            if ($field->isMulti()) {
                                $value = json_encode($value);
                            }

                            $this->setValue($fieldKey, $value);
                        }
                    }
                    // If the field is set to reference another field
                    elseif (is_string($fieldValue) && strpos($fieldValue, self::REFERENCE_FIELD_MARKER) === 0) {
                        $this->getField($fieldKey)->setReferenceField(trim($fieldValue, self::REFERENCE_FIELD_MARKER));
                    }
                    // Otherwise just set the value
                    else {
                        $this->setValue($fieldKey, $fieldValue);
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Takes a json-encoded string config, fills any reference fields with their values, and returns a json-encoded string.
     * @param string $json
     * @param array $values array of key to value pairs
     * @return string the json encoded result
     * @throws ConfigurationException
     */
    public static function fillReferenceFields ($json, $values) {
        $data = json_decode($json, true);

        if ($data === FALSE) {
            throw new ConfigurationException('Json could not be decoded: ' . json_last_error_msg(), json_last_error());
        }

        if (is_array($data)) {
            foreach ($data as $fieldKey => $fieldValue) {
                // If this field is a reference field
                if (is_string($fieldValue) && strpos($fieldValue, self::REFERENCE_FIELD_MARKER) !== false) {
                    $referenceKey = trim($fieldValue, self::REFERENCE_FIELD_MARKER);

                    if (! array_key_exists($referenceKey, $values)) {
                        throw new ConfigurationException('Could not find a value for reference field: ' . $referenceKey, 404);
                    }

                    $fieldValue = $values[$referenceKey];
                }

                $data[$fieldKey] = $fieldValue;
            }
        }

        return json_encode($data);
    }

    /**
     * @return mixed
     */
    public function getPublicEncryptKey () {
        return $this->publicEncryptKey;
    }

    /**
     * @param mixed $publicEncryptKey
     */
    public function setPublicEncryptKey ($publicEncryptKey) {
        $this->publicEncryptKey = $publicEncryptKey;
    }

    /**
     * @return mixed
     */
    public function getPrivateDecryptKey () {
        return $this->privateDecryptKey;
    }

    /**
     * @param mixed $privateDecryptKey
     */
    public function setPrivateDecryptKey ($privateDecryptKey) {
        $this->privateDecryptKey = $privateDecryptKey;
    }
}